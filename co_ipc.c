#include "timers_driver.h"
#include <pthread.h>

extern pthread_mutex_t mutex;

void enter_mutex(void) {
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    if (pthread_mutex_lock(&mutex) != 0) {
        printf("pthread_mutex_lock() failed\n");
    }
#endif
}

void leave_mutex(void) {
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    if (pthread_mutex_unlock(&mutex) != 0) {
        printf("pthread_mutex_unlock() failed\n");
    }
#endif
}





