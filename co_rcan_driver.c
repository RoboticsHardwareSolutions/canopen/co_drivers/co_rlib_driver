#include "rtimer.h"
#include "timers_driver.h"
#include "data.h"
#include "rcan.h"
#include <pthread.h>
#include <string.h>
#include "can_driver.h"


#ifdef BUFFER_EXIST
#include "rbuffer.h"
rbuffer buffer;
rcan_frame rcan_frames[16];
#endif


struct can_param
{
    CAN_PORT can_port;
    uint32_t bitrate;
    CO_Data *data;
};


static pthread_t can_thread;
static pthread_mutex_t mutex_quit;
pthread_mutex_t mutex;

static void leave_mutex_qiut(void);

static void enter_mutex_quit(void);

static volatile bool quit = false;
static rcan can1, can2;
rcan *can;
struct can_param execute_params;
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)

static void can_loop(CAN_PORT port, uint32_t bitrate, CO_Data *d);

#endif

void *can_thread_loop(void *arg)
{
    if (arg == NULL)
    {
        MSG("invalid arg in can_thread_loop\r\n");
        return NULL;
    }

    struct can_param *param = (struct can_param *) arg;
    MSG("can loop arg - port = %d, bitrate = %d\r\n", param->can_port, param->bitrate);
    can_loop(param->can_port, param->bitrate, param->data);
    return NULL;
}

int canSend(CAN_PORT port, Message const *m)
{

    if (port == 0 || m == NULL)
        return 1;

    rcan_frame frame = {0};
    frame.id = m->cob_id;
    frame.type = std_id;
    frame.len = m->len;
    frame.rtr = m->rtr;
    memcpy(frame.payload, m->data, m->len);

#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    can = &can1;
#else
#ifdef CAN
    can = &can1;
#else
    if (port == CAN1)
        can = &can1;
    else can = &can2;
#endif
#endif

    if (rcan_send(can, &frame) != true)
    {
        #ifdef BUFFER_EXIST
        rbuffer_push(&buffer, (uint8_t*) &frame, sizeof(frame));
        return 0;
        #endif
        return 1;
    }
    return 0;
}

int canOpen(CAN_PORT port, uint32_t bitrate, CO_Data *d)
{
#ifdef BUFFER_EXIST
    rbuffer_create(&buffer, (uint8_t*) &rcan_frames, sizeof(rcan_frames));
#endif
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    can = &can1;
    execute_params.can_port = port;
    execute_params.bitrate = bitrate;
    execute_params.data = d;
    pthread_mutex_init(&mutex_quit, NULL);
    pthread_mutex_init(&mutex, NULL);
    if (pthread_create(&can_thread, NULL, &can_thread_loop, &execute_params) != 0)
    {
        printf("can thread error create \r\n");
        return -1;
    }
    return 0;
#else
    d->canHandle = port;
#ifdef CAN
    can = &can1;
#else
    if (port == CAN1)
        can = &can1;
    else can = &can2;
#endif
    return rcan_start(can, (UNS32) port, bitrate);
#endif

}

__attribute__((weak)) void weak_canDispatch(CO_Data* d, Message *m) {
    (void) d;
    (void) m;
}


void can_loop(CAN_PORT port, uint32_t bitrate, CO_Data *d)
{

#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    can = &can1;
    d->canHandle = port;

    if (!rcan_start(can, (uint32_t) port, bitrate))
    {
        printf("rcan not start\r\n");
        exit(EXIT_SUCCESS);//TODO to main
        return;
    }

    while(1)
    {

        enter_mutex_quit();
        if (quit)
            break;

        leave_mutex_qiut();

        usleep(100);
        Message rxm = {0};
        rcan_frame frame = {0};

        uint32_t code = CAN_GetStatus(can->channel);
        if (code)
        {
            char res[100];
            CAN_GetErrorText(
                code,
                0x09,
                res);

            //MSG_WAR(code, res, 0x00);
        }

        if (!rcan_is_ok(can))
        {
            rcan_stop(can);
            rcan_start(can, (uint32_t) port, bitrate);
        }
        //printf("can loop executing\r\n");
        if (rcan_receive(can, &frame))
        {
            rxm.cob_id = frame.id;
            rxm.rtr = frame.rtr;
            rxm.len = frame.len;
            memcpy(rxm.data, frame.payload, frame.len);
            enter_mutex();
            canDispatch(d, &rxm);
            leave_mutex();
            //rcan_view_frame(&frame);
        }
    }

    d->canHandle = NULL;
    //d->canBitrate = 0;
    rcan_stop(d->canHandle);
    printf("can loop break\r\n");
#else
    Message rxm = {0};
    rcan_frame frame = {0};
#ifdef CAN
    can = &can1;
#else
    if (port == CAN1)
        can = &can1;
    else can = &can2;
#endif


    if (!rcan_is_ok(can))
    {
        if (can->errors == CE_EPV)
        {
            rcan_stop(can);
            rcan_start(can, (uint32_t) port, bitrate);
        }
        if(can->errors != CE_OK)
        {
            EMCY_setError(d, 0x8100, can->errors, 0x00);
        }
    }
    else
    {
        EMCY_errorRecovered(d, 0x8100);
    }

    //printf("can loop executing\r\n");
    if (rcan_receive(can, &frame))
    {
        rxm.cob_id = frame.id;
        rxm.rtr = frame.rtr;
        rxm.len = frame.len;
        memcpy(rxm.data, frame.payload, frame.len);
        enter_mutex();
        canDispatch(d, &rxm);
        weak_canDispatch(d, &rxm);
        leave_mutex();
        //rcan_view_frame(&frame);
    }

#ifdef BUFFER_EXIST
    rcan_frame frame_tx;
    Message m;
    if(rbuffer_is_empty(&buffer))
    {
        return;
    }
    rbuffer_pop(&buffer, (uint8_t*) &frame_tx, sizeof(frame_tx));

    m.cob_id = frame_tx.id;
    m.len = frame_tx.len;
    m.rtr = frame_tx.rtr;
    memcpy(m.data, frame_tx.payload, m.len);
    canSend(CAN1, &m);
#endif

#endif

}


int canClose(CO_Data *d)
{
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    enter_mutex();
    quit = true;
    leave_mutex();
    pthread_join(can_thread, NULL);
#endif
    return 0;
}


static void enter_mutex_quit(void)
{
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    if (pthread_mutex_lock(&mutex_quit) != 0)
    {
        printf("pthread_mutex_quit_lock() failed\n");
    }
#endif
}

static void leave_mutex_qiut(void)
{
#if defined(__unix__) || defined(__APPLE__) || defined(__CYGWIN__)
    if (pthread_mutex_unlock(&mutex_quit) != 0)
    {
        printf("pthread_mutex_quit_unlock() failed\n");
    }
#endif
}
