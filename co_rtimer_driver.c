#include "rtimer.h"
#include "timers_driver.h"
#include "timer.h"
#include "data.h"
#include "can_driver.h"

static rtimer timer;

void timer_notify(void)
{
    //printf("timer cb\r\n");
    enter_mutex();
    TimeDispatch();
    leave_mutex();
}

void TimerInit(void)
{
    if (!rtimer_create(&timer))
    {
#if NOT_ARM
        fprintf(stderr, "mutex create failed\n");
#endif
    }
}

void TimerCleanup(void)
{
    if (!rtimer_delete(&timer)) {
    }
#if NOT_ARM
    fprintf(stderr, "mutex create failed\n");
#endif
}

void StartTimerLoop(TimerCallback_t init_callback)
{
    enter_mutex();
    rtimer_setup(&timer, 500, timer_notify);
    SetAlarm(NULL, 0, init_callback, MS_TO_TIMEVAL(1500), 0);
    leave_mutex();
}

void StopTimerLoop(TimerCallback_t exitfunction)
{
    enter_mutex();
    rtimer_delete(&timer);
    exitfunction(NULL, 0);
    leave_mutex();
}


void setTimer(TIMEVAL value)
{
    //printf("setTimer called\r\n");
    if (!rtimer_setup(&timer, value, timer_notify))
    {
#if NOT_ARM
        fprintf(stderr, "timer setup failed\n");
#endif
    }
}


TIMEVAL getElapsedTime(void)
{
    return rtimer_get_elapsed_time(&timer);
}
